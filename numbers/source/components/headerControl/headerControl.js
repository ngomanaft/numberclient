import React from 'react';
import {
    View, TextInput, TouchableHighlight, Text, StyleSheet
} from 'react-native';

const headerControl = props => {
    return(
        <View>
            <View style={styles.container}>
                <TouchableHighlight onPress={props.createQuoteButton}>
                    <Text>Create Quote</Text>
                </TouchableHighlight>
                <Text>{props.userCompanyName}</Text>
                <TouchableHighlight onPress={props.createInvoiceButton}>
                    <Text>Create Invoice</Text>
                </TouchableHighlight>
            </View>
        </View>
    )
};

const styles =  StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 5,
        marginRight: 5,
        paddingTop: 20
    }
})

export default headerControl;