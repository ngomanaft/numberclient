import React from 'react';
import {
    View, Text, TextInput, StyleSheet
} from 'react-native';

//importing components
import HeaderControlCenter from '../components/headerControl/headerControl';

export default class MainScreen extends React.Component {
    state = {

    };

    createQuotationHandler = e => {
        return alert('this will trigger a create quotation modal');
    };
    render() {
        return (
            <View>
                <HeaderControlCenter
                    userCompanyName='Numbers Group'
                    createQuoteButton={this.createQuotationHandler}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});